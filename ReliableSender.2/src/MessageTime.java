public class MessageTime {
    private Message msg;
    private long time;

    MessageTime(Message msg) {
        this.msg = new Message(msg);
        setUpdatedTime();
    }

    public Message getMessage() { return msg; }

    public long getTime() { return time; }

    public void setUpdatedTime() { time = System.currentTimeMillis(); }

    public boolean equals(Object obj) {
        return ((obj instanceof MessageTime) && this.msg.equals(((MessageTime) obj).msg));
    }
}
