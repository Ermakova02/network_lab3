import java.io.IOException;
import java.net.*;
import java.util.*;

public class Node {
    public static final int BUFFER_SIZE = 1024;
    public static final long MESSAGES_PROCESSING_DELAY = 50;
    public static final long MESSAGES_LIFE_PERIOD = 30000;

    private String nodeName;
    private int port;
    private int lossPercent;
    private String parentIP;
    private int parentPort;
    private boolean isRoot;
    private Random random;
    private List<NodeInfo> children;
    private List<MessageTime> sentMessages;
    private List<MessageTime> receivedMessages;

    private void InitData(String nodeName, int lossPercent, int port) {
        this.nodeName = new String(nodeName);
        this.lossPercent = lossPercent;
        this.port = port;
        parentIP = "";
        parentPort = 0;
        isRoot = true;
        random = new Random();
        children = new ArrayList<>();
        sentMessages = new ArrayList<>();
        receivedMessages = new ArrayList<>();
    }

    Node(String nodeName, int lossPercent, int port) {
        InitData(nodeName, lossPercent, port);
    }

    Node(String nodeName, int lossPercent, int port, String parentIP, int parentPort) {
        InitData(nodeName, lossPercent, port);
        isRoot = false;
        this.parentIP = parentIP;
        this.parentPort = parentPort;
    }

    public String getNodeName() { return nodeName; }

    public int getPort() { return port; }

    public boolean losePacket() {
        return (lossPercent > random.nextInt(100));
    }

    public void toByteArrayInt(int value, byte[] data, int pos) {
        data[pos] = (byte)(value >> 24);
        data[pos + 1] = (byte)(value >> 16);
        data[pos + 2] = (byte)(value >> 8);
        data[pos + 3] = (byte)(value);
    }

    public int fromByteArrayInt(byte[] data, int pos) {
        return data[pos] << 24 | (data[pos + 1] & 0xFF) << 16 | (data[pos + 2] & 0xFF) << 8 | (data[pos + 3] & 0xFF);
    }

    public void toByteArrayLong(long value, byte[] data, int pos) {
        data[pos] = (byte)(value >> 56);
        data[pos + 1] = (byte)(value >> 48);
        data[pos + 2] = (byte)(value >> 40);
        data[pos + 3] = (byte)(value >> 32);
        data[pos + 4] = (byte)(value >> 24);
        data[pos + 5] = (byte)(value >> 16);
        data[pos + 6] = (byte)(value >> 8);
        data[pos + 7] = (byte)(value);
    }

    public long fromByteArrayLong(byte[] data, int pos) {
        return ((long)(data[pos]) << 56) |
                ((long)(data[pos + 1]) & 0xFF) << 48 |
                ((long)(data[pos + 2]) & 0xFF) << 40 |
                ((long)(data[pos + 3]) & 0xFF) << 32 |
                ((long)(data[pos + 4]) & 0xFF) << 24 |
                ((long)(data[pos + 5]) & 0xFF) << 16 |
                ((long)(data[pos + 6]) & 0xFF) << 8 |
                ((long)(data[pos + 7]) & 0xFF);
    }

    public UUID fromByteArrayUUID(byte[] data, int pos) {
        long mostSigBits = fromByteArrayLong(data, pos);
        long leastSigBits = fromByteArrayLong(data, pos + 8);
        UUID uuid = new UUID(mostSigBits, leastSigBits);
        return uuid;
    }

    public void toByteArrayUUID(UUID uuid, byte[] data, int pos) {
        long mostSigBits = uuid.getMostSignificantBits();
        long leastSigBits = uuid.getLeastSignificantBits();
        toByteArrayLong(mostSigBits, data, pos);
        toByteArrayLong(leastSigBits, data, pos + 8);
    }

    /*
    0 (4) - Message Type: MESSAGE | NEW_CHILD | CONFIRMATION; int
    4 (16) - Message UUID; UUID
    20 (4) - Node Name Length (NNL); int
    24 (NNL) - Node Name; String
    24 + NNL (4) - Port From; int
    28 + NNL (4) - Message Length; int
    32 + NNL + (ML) - Message; String
    */

    public byte [] PackMessage(Message msg) {
        int nodeNameLength = msg.nodeName.getBytes().length;
        int messageLength = msg.message.getBytes().length;
        byte [] data = new byte[32 + nodeNameLength + messageLength];
        toByteArrayInt(msg.type.getValue(), data, 0);
        toByteArrayUUID(msg.uuid, data, 4);
        toByteArrayInt(nodeNameLength, data, 20);
        System.arraycopy(msg.nodeName.getBytes(), 0, data, 24, nodeNameLength);
        toByteArrayInt(msg.portFrom, data, 24 + nodeNameLength);
        toByteArrayInt(messageLength, data, 28 + nodeNameLength);
        System.arraycopy(msg.message.getBytes(), 0, data, 32 + nodeNameLength, messageLength);
        return data;
    }

    public Message UnpackMessage(byte [] data) {
        int id = fromByteArrayInt(data, 0);
        MessageTypesEnum type = MessageTypesEnum.fromInteger(id);
        UUID uuid = fromByteArrayUUID(data, 4);
        int nodeNameLength = fromByteArrayInt(data, 20);
        String nodeName = new String(data, 24, nodeNameLength);
        int portFrom = fromByteArrayInt(data, 24 + nodeNameLength);
        int messageLength = fromByteArrayInt(data, 28 + nodeNameLength);
        String message = new String(data, 32 + nodeNameLength, messageLength);
        Message msg = new Message(type, nodeName, "", 0, portFrom, message, uuid);
        return msg;
    }

    private void ResendMessage(Message m) {
        DatagramSocket sendSocket = null;
        try {
            sendSocket = new DatagramSocket();
            SendMessageBySocket(sendSocket, m);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            sendSocket.close();
        }
    }

    private synchronized void CastMessage(Message m, String excludeAddress, int excludePort) {
        Message msg = new Message(m);
        msg.portFrom = port;
        DatagramSocket sendSocket = null;
        try {
            sendSocket = new DatagramSocket();
            if (!isRoot)
                if (!(parentIP.equals(excludeAddress) & parentPort == excludePort)) {
                    msg.address = new String(parentIP);
                    msg.port = parentPort;
                    msg.uuid = UUID.randomUUID();
                    SendMessageBySocket(sendSocket, msg);
                    AddSentMessage(msg);
                }
            for (int i = 0; i < children.size(); i++) {
                msg.address = new String(children.get(i).address);
                msg.port = children.get(i).port;
                if (!(msg.address.equals(excludeAddress) & msg.port == excludePort)) {
                    msg.uuid = UUID.randomUUID();
                    SendMessageBySocket(sendSocket, msg);
                    AddSentMessage(msg);
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            sendSocket.close();
        }
    }

    private synchronized boolean IsInChildren(Message msg, String address, int port) {
        NodeInfo child = new NodeInfo(msg.nodeName, address, port);
        return children.contains(child);
    }

    private synchronized void AddChild(Message msg, String address, int port) {
        NodeInfo child = new NodeInfo(msg.nodeName, address, port);
        if (!children.contains(child)) children.add(child);
    }

    private synchronized void HandleConfirmation(Message msg) {
        for (int i = 0; i < sentMessages.size(); i++) {
            if (sentMessages.get(i).getMessage().uuid.equals(msg.uuid)) {
                sentMessages.remove(sentMessages.get(i));
                break;
            }
        }
    }

    private void SendConfirmation(String address, int portFrom, UUID uuid) {
        Message msg = new Message(MessageTypesEnum.CONFIRMATION, nodeName, address, portFrom, port, "", uuid);
        DatagramSocket sendSocket = null;
        try {
            sendSocket = new DatagramSocket();
            SendMessageBySocket(sendSocket, msg);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            sendSocket.close();
        }
    }

    public void newDataReceived(Message msg, String address) {
        msg.address = new String(address);
        switch (msg.type) {
            case MESSAGE:
                if (!InReceivedMessages(msg)) {
                    System.out.println(msg.nodeName + ": " + msg.message);
                    CastMessage(msg, address, msg.portFrom);
                    AddReceivedMessage(msg);
                }
                SendConfirmation(address, msg.portFrom, msg.uuid);
                break;
            case NEW_CHILD:
                if (!IsInChildren(msg, address, msg.portFrom)) {
                    AddChild(msg, address, msg.portFrom);
                    AddReceivedMessage(msg);
                }
                SendConfirmation(address, msg.portFrom, msg.uuid);
                break;
            case CONFIRMATION:;
                HandleConfirmation(msg);
                break;
        }
    }

    private synchronized void AddSentMessage(Message msg) {
        sentMessages.add(new MessageTime(msg));
    }

    private synchronized void AddReceivedMessage(Message msg) {
        receivedMessages.add(new MessageTime(msg));
    }

    private synchronized boolean InReceivedMessages(Message msg) {
        for (int i = 0; i < receivedMessages.size(); i++)
            if (receivedMessages.get(i).getMessage().uuid.equals(msg.uuid))
                return true;
        return false;
    }

    private void SendMessageBySocket(DatagramSocket sendSocket, Message msg) throws IOException {
        byte [] data = PackMessage(msg);
        DatagramPacket packet = new DatagramPacket(data, data.length);
        packet.setAddress(InetAddress.getByName(msg.address));
        packet.setPort(msg.port);
        sendSocket.send(packet);
    }

    public synchronized void newMessageToSend(String newMessage) {
        if (isRoot & children.size() == 0) return;
        DatagramSocket sendSocket = null;
        try {
            sendSocket = new DatagramSocket();
            Message msg = new Message(MessageTypesEnum.MESSAGE, nodeName, parentIP, parentPort, port, newMessage, UUID.randomUUID());
            if (!isRoot) {
                SendMessageBySocket(sendSocket, msg);
                AddSentMessage(msg);
            }
            for (int i = 0; i < children.size(); i++) {
                msg.uuid = UUID.randomUUID();
                msg.address = new String(children.get(i).address);
                msg.port = children.get(i).port;
                SendMessageBySocket(sendSocket, msg);
                AddSentMessage(msg);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            sendSocket.close();
        }
    }

    public synchronized void ProcessReceivedMessages() {
        long time = System.currentTimeMillis();
        for (int i = 0; i < receivedMessages.size(); i++) {
            if (time - receivedMessages.get(i).getTime() > MESSAGES_LIFE_PERIOD) {
                receivedMessages.remove(receivedMessages.get(i));
            }
        }
    }

    public synchronized void RemoveConnectedNodeByMessage(Message msg) {
        if (!isRoot) {
            if (parentIP.equals(msg.address) & parentPort == msg.port) {
                System.err.println("Remove parent node on address=" + parentIP + ", port=" + Integer.toString(parentPort));
                parentPort = 0;
                parentIP = "";
                isRoot = true;
            }
        }
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i).address.equals(msg.address) & children.get(i).port == msg.port) {
                System.err.println("Remove child node \"" + children.get(i).name + "\" on address=" + children.get(i).address + ", port=" + Integer.toString(children.get(i).port));
                children.remove(children.get(i));
            }
        }
    }

    public synchronized void ProcessSentMessages() {
        long time = System.currentTimeMillis();
        for (int i = 0; i < sentMessages.size(); i++) {
            if (time - sentMessages.get(i).getTime() > MESSAGES_LIFE_PERIOD) {
                RemoveConnectedNodeByMessage(sentMessages.get(i).getMessage());
                sentMessages.remove(sentMessages.get(i));
            }
        }
        for (int i = 0; i < sentMessages.size(); i++) {
            ResendMessage(sentMessages.get(i).getMessage());
        }
    }

    private void SendNotificationToParent() {
        if (isRoot) return;
        DatagramSocket sendSocket = null;
        try {
            sendSocket = new DatagramSocket();
            Message msg = new Message(MessageTypesEnum.NEW_CHILD, nodeName, parentIP, parentPort, port, "", UUID.randomUUID());
            SendMessageBySocket(sendSocket, msg);
            AddSentMessage(msg);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            sendSocket.close();
        }
    }

    public void launch() {
        SendNotificationToParent();
        new ReceivedMessagesProcessor(this);
        new SentMessagesProcessor(this);
        new Listener(this);
        new MessageSender(this);
    }
}
