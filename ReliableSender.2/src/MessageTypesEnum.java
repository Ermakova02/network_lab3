public enum MessageTypesEnum {
    MESSAGE(0),
    NEW_CHILD(1),
    CONFIRMATION(2);

    private final int id;
    MessageTypesEnum(int id) { this.id = id; }
    public int getValue() { return id; }
    public static MessageTypesEnum fromInteger(int x) {
        switch(x) {
            case 0:
                return MESSAGE;
            case 1:
                return NEW_CHILD;
            case 2:
                return CONFIRMATION;
        }
        return null;
    }
}
