import java.util.Scanner;

public class MessageSender implements Runnable {
    private Thread t;
    private Node node;

    MessageSender(Node node) {
        this.node = node;
        t = new Thread(this, "Sender " + this.node.getNodeName());
        t.start();
    }

    @Override
    public void run() {
        Scanner in = new Scanner(System.in);
        while(true)
        {
            String messageStr = in.nextLine();
            node.newMessageToSend(messageStr);
        }
    }
}
