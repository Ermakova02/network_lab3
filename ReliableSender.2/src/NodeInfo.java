public class NodeInfo {
    public String name;
    public int port;
    public String address;

    NodeInfo(String name, String address, int port) {
        this.name = new String(name);
        this.port = port;
        this.address = new String(address);
    }

    public boolean equals(Object obj) {
        return ((obj instanceof NodeInfo) &&
                this.address.equals(((NodeInfo) obj).address) &&
                this.port == ((NodeInfo) obj).port
        );
    }

}
