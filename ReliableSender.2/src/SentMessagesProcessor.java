public class SentMessagesProcessor implements Runnable {
    private Thread t;
    private Node node;
    SentMessagesProcessor(Node node) {
        this.node = node;
        t = new Thread(this, "ReceivedMessagesProcessor " + this.node.getNodeName());
        t.start();
    }
    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(node.MESSAGES_PROCESSING_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            node.ProcessSentMessages();
        }
    }
}
