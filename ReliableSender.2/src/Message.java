import java.net.InetAddress;
import java.util.UUID;

public class Message {
    public MessageTypesEnum type;
    public String nodeName;
    public String address;
    public int port;
    public int portFrom;
    public String message;
    public UUID uuid;

    Message(MessageTypesEnum type, String nodeName, String address, int port, int portFrom, String message, UUID uuid) {
        this.type = type;
        this.nodeName = new String(nodeName);
        this.address = new String(address);
        this.port = port;
        this.portFrom = portFrom;
        this.message = new String(message);
        this.uuid = new UUID(uuid.getMostSignificantBits(), uuid.getLeastSignificantBits());
    }

    Message(Message msg) {
        type = msg.type;
        nodeName = new String(msg.nodeName);
        address = new String(msg.address);
        port = msg.port;
        portFrom = msg.portFrom;
        message = new String(msg.message);
        uuid = new UUID(msg.uuid.getMostSignificantBits(), msg.uuid.getLeastSignificantBits());
    }

    public String toString() {
        return "Message [type=" + type.toString()
                + ", nodeName=" + nodeName
                + ", address=" + address
                + ", port=" + Integer.toString(port)
                + ", portFrom=" + Integer.toString(portFrom)
                + ", message=" + message
                + ", uuid=" + uuid.toString() + "]";
    }

    public boolean equals(Object obj) {
        return ((obj instanceof Message) &&
                this.type.equals(((Message) obj).type) &&
                this.nodeName.equals(((Message) obj).nodeName) &&
                this.address.equals(((Message) obj).address) &&
                (this.port == ((Message) obj).port) &&
                this.message.equals(((Message) obj).message) &&
                this.uuid.equals(((Message) obj).uuid)
        );
    }
}
