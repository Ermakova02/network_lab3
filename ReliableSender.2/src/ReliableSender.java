public class ReliableSender {
    public static void main(String[] args) {
        if (args.length != 3 & args.length != 5) {
            System.out.println("Usage: java ReliableSender [node name] [loss %] [port number] {[parent IP] [parent port]}");
            return;
        }
        Node node = null;
        String nodeName = args[0];
        int lossPercent = Integer.valueOf(args[1]);
        int port = Integer.valueOf(args[2]);
        String parentIP = "";
        int parentPort = 0;
        if (args.length == 5) {
            parentIP = args[3];
            parentPort = Integer.valueOf(args[4]);
            node = new Node(nodeName, lossPercent, port, parentIP, parentPort);
        }
        else node = new Node(nodeName, lossPercent, port);
        node.launch();
    }
}
