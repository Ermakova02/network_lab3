import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Listener implements Runnable {
    private Thread t;
    private Node node;

    Listener(Node node) {
        this.node = node;
        t = new Thread(this, "Listener " + this.node.getNodeName());
        t.start();
    }

    @Override
    public void run() {
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket(node.getPort());
            byte[] buffer = new byte[node.BUFFER_SIZE];
            DatagramPacket rcvPacket = new DatagramPacket(buffer, buffer.length);
            while (true) {
                socket.receive(rcvPacket);
                if (node.losePacket()) continue;
                Message msg = node.UnpackMessage(rcvPacket.getData());
                String address = rcvPacket.getAddress().getHostAddress();
                node.newDataReceived(msg, address);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }
    }
}
